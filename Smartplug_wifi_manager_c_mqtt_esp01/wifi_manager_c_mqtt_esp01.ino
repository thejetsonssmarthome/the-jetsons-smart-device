#include <FS.h>                   //this needs to be first, or it all crashes and burns...
//#include "SPIFFS.h"
#if defined(ESP8266)
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#else
#include <WiFi.h>          //https://github.com/esp8266/Arduino
#endif
#include <PubSubClient.h>
#include <Wire.h>

#include <DNSServer.h>
#if defined(ESP8266)
#include <ESP8266WebServer.h>
#else
#include <WebServer.h>
#endif
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson
#include <ESP8266TrueRandom.h>    //https://github.com/marvinroger/ESP8266TrueRandom

// Set web server port number to 80
WiFiServer server(80);
WiFiClient espClient;
PubSubClient client(espClient);

void on_connect();
void callback(char* topic, byte* payload, unsigned int length);
void send_heartbeat();
void update_status(int new_status);
void send_status();
void set_topics();
void set_device();


String header; // Variable to store the HTTP request
bool shouldSaveConfig = false; //flag for saving data
String outputState = "off";// Auxiliar variables to store the current output state
char switch_topic[80];
StaticJsonBuffer<200> jsonBuffer;
JsonObject& device = jsonBuffer.createObject();
String json_device_string;
// Assign output variables to GPIO pins
char output[2] = "4";
//define your default values here, if there are different values in config.json, they are overwritten.
char mqtt_server[40] = "192.168.15.8";
char mqtt_port[6] = "1883";
char device_group[34] = "Quarto";
char device_name[34] = "Luz";
char device_id[16] = ""; // UUIDs in binary form are 16 bytes long
long last_heartbeat = 0;


//callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

void setup() {
  Serial.begin(115200);

  byte uuidNumber[16];
  ESP8266TrueRandom.uuid(uuidNumber);
  strcpy(device_id, ESP8266TrueRandom.uuidToString(uuidNumber).c_str());
  Serial.println("UUID" + ESP8266TrueRandom.uuidToString(uuidNumber));
  //clean FS, for testing
  SPIFFS.format();

  //read configuration from FS json
  Serial.println("mounting FS...");

  if (SPIFFS.begin()) {
    Serial.println("mounted file system");
    if (SPIFFS.exists("/config_esp.json")) {
      //file exists, reading and loading
      Serial.println("reading config file");
      File configFile = SPIFFS.open("/config_esp.json", "r");
      if (configFile) {
        Serial.println("opened config file");
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        json.printTo(Serial);
        if (json.success()) {
          Serial.println("\nparsed json");
          //strcpy(output, json["output"]);
          strcpy(mqtt_server, json["mqtt_server"]);
          strcpy(mqtt_port, json["mqtt_port"]);
          strcpy(device_group, json["device_group"]);
          strcpy(device_name, json["device_name"]);
          strcpy(device_id, json["device_id"]);
        } else {
          Serial.println("failed to load json config");
        }
      }
    }
  } else {
    Serial.println("failed to mount FS");
  }
  //end read

  //WiFiManagerParameter custom_output("output", "output", output, 2);
  WiFiManagerParameter custom_mqtt_server("server", "mqtt server", mqtt_server, 40);
  WiFiManagerParameter custom_mqtt_port("port", "mqtt port", mqtt_port, 6);
  WiFiManagerParameter custom_device_group("device_group", "device_group", device_group, 32);
  WiFiManagerParameter custom_device_name("device_name", "device_name", device_name, 32);

  // WiFiManager

  WiFiManager wifiManager;// Local intialization. Once its business is done, there is no need to keep it around
  wifiManager.setSaveConfigCallback(saveConfigCallback);  //set config save notify callback
  //wifiManager.setSTAStaticIPConfig(IPAddress(10,0,1,99), IPAddress(10,0,1,1), IPAddress(255,255,255,0)); // set custom ip for portal

  //add all your parameters here
  //wifiManager.addParameter(&custom_output);
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_port);
  wifiManager.addParameter(&custom_device_group);
  wifiManager.addParameter(&custom_device_name);

  //wifiManager.resetSettings(); // Uncomment and run it once, if you want to erase all the stored information
  //wifiManager.setMinimumSignalQuality(); //set minimu quality of signal so it ignores AP's under that quality defaults to 8%
  //wifiManager.setTimeout(120); //sets timeout until configuration portal gets turned off, useful to make it all retry or go to sleep in seconds

  // fetches ssid and pass from eeprom and tries to connect, if it does not connect it starts an access point with the specified name here  "AutoConnectAP"
  // and goes into a blocking loop awaiting configuration
  //wifiManager.autoConnect("AutoConnectAP");
  //wifiManager.autoConnect(); // or use this for auto generated name ESP + ChipID

  if (!wifiManager.autoConnect("ESP_CAU_ZIMMER")) {
    Serial.println("failed to connect and hit timeout");
    delay(3000);
    ESP.restart(); //reset and try again, or maybe put it to deep sleep
    delay(5000);
  }

  Serial.println("Connected."); // if you get here you have connected to the WiFi

  //strcpy(output, custom_output.getValue());
  strcpy(mqtt_server, custom_mqtt_server.getValue());
  strcpy(mqtt_port, custom_mqtt_port.getValue());
  strcpy(device_group, custom_device_group.getValue());
  strcpy(device_name, custom_device_name.getValue());

  //save the custom parameters to FS
  if (shouldSaveConfig) {
    Serial.println("saving config");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
    json["output"] = output;
    json["mqtt_server"] = mqtt_server;
    json["mqtt_port"] = mqtt_port;
    json["device_group"] = device_group;
    json["device_name"] = device_name;
    json["device_id"] = device_id;

    File configFile = SPIFFS.open("/config_esp.json", "w");
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }

    json.printTo(Serial);
    json.printTo(configFile);
    configFile.close();
    //end save

    pinMode(atoi(output), OUTPUT); // Initialize the output variables as outputs
    digitalWrite(atoi(output), LOW); // Set outputs to LOW

    server.begin(); //webserver p requisições diretas
    client.setServer(mqtt_server, atoi(mqtt_port));
    client.setCallback(callback);

  }
  set_topics();
  set_device();
}

void set_topics() {
  strcpy(switch_topic, "/DEVICE/");
  strcat(switch_topic, device_id);
  strcat(switch_topic, "/SWITCH");
}

void set_device() {
  device["device_id"] = device_id;
  device["device_name"] = device_name;
  device["device_group"] = device_group;
  device["device_type"] = "SWITCH";
  device["device_status"] = 0;
  device.printTo(json_device_string);
}

void update_status(int new_status){
  device["device_status"] = new_status;
  device.printTo(json_device_string);
}

void send_heartbeat(){
   client.publish("/DEVICE/HEARTBEAT", json_device_string.c_str());
}

void send_status(){
   client.publish("/DEVICE/STATUS", json_device_string.c_str());
}

void callback(char* topic, byte* message, unsigned int length) {
  Serial.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n RECEBENDO MSG... \n TOPICO:");
  Serial.printf("\n ID ORIGEM MSG: %c ", message[0]);
  char sinal = (char)message[0];
  int msg_sinal = (int) message[0] - 48;
  if (strcmp(topic, switch_topic) == 0 ) {
    Serial.println(topic);
    if (msg_sinal == 1 ) {
      Serial.println("ligando...");
      digitalWrite(atoi(output), HIGH);      
    } else if (msg_sinal == 0 ) {
      Serial.println("apagando...");
      digitalWrite(atoi(output), LOW);      
    }
    update_status(msg_sinal);
    send_status();
  }
}

void reconnect() {
  while (!client.connected()) {
    Serial.println("Attempting MQTT connection...");
    if (client.connect(device_id)) {
      Serial.println("connected");
      client.subscribe(switch_topic);
      client.publish("/DEVICE/NEW", json_device_string.c_str());
    } else {
      Serial.println("failed, rc=");
      Serial.println(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}


void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis();
  if (now - last_heartbeat > 60000) { // 1 minuto
    last_heartbeat = now;
    send_heartbeat();
  }
}
