#include <FS.h>                   //this needs to be first, or it all crashes and burns...

#if defined(ESP8266)
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#else
#include <WiFi.h>          //https://github.com/esp8266/Arduino
#endif
#include <PubSubClient.h>
#include <Wire.h>

#include <DNSServer.h>
#if defined(ESP8266)
#include <ESP8266WebServer.h>
#else
#include <WebServer.h>
#endif
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

// MQTT_MAX_PACKET_SIZE : Maximum packet size
#undef MQTT_MAX_PACKET_SIZE
#undef MQTT_MAX_TRANSFER_SIZE
#define MQTT_MAX_PACKET_SIZE 1024
#define MQTT_MAX_TRANSFER_SIZE 1024

// Set web server port number to 80
WiFiServer server(80);
WiFiClient espClient;
PubSubClient client(espClient);

void on_connect();
void callback(char* topic, byte* payload, unsigned int length);
void send_heartbeat();
void update_status(int new_status);
void send_status();
void set_topics();
void set_device();
void setup_wifi();
void send_new_device();

char switch_topic[80];
StaticJsonBuffer<400> jsonBuffer;
JsonObject& device = jsonBuffer.createObject();
String str_device;
char output[2] = "4";

const char* ssid = "Luisa";
const char* password = "amorzinho*123";

char* mqtt_server = "192.168.15.8";
char* mqtt_port = "1883";

char* device_group = "Quarto";
char* device_name = "Luz";
String device_mac;
long last_heartbeat = 0;

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  device_mac = WiFi.macAddress();
  //strcpy(device_mac, WiFi.macAddress().c_str());
  Serial.println("MAC_ADDRESS: ");
  Serial.println(device_mac);

}

void setup() {
  Serial.begin(115200);
  setup_wifi();

  Serial.println("Configurando GPIO...");
  pinMode(atoi(output), OUTPUT); // Initialize the output variables as outputs
  digitalWrite(atoi(output), LOW); // Set outputs to LOW

  Serial.println("Configurando WEBSERVER...");
  server.begin();
  Serial.println("Configurando MQTTSERVER...");
  client.setServer(mqtt_server, atoi(mqtt_port));
  client.setCallback(callback);

  set_topics();
  set_device();
}

void set_topics() {
  Serial.println("Configurando topicos...");
  strcpy(switch_topic, "/DEVICE/");
  strcat(switch_topic, device_mac.c_str());
  strcat(switch_topic, "/SWITCH");
}

void set_device() {
  Serial.println("Configurando os dados do device...");
  device["id"] = device_mac;
  device["name"] = device_name;
  device["group"] = device_group;
  device["type"] = "SWITCH";
  device["status"] = 0;

  device.printTo(str_device);
  Serial.println(str_device);
}

void update_status(int new_status) {
  Serial.println("Atualizando status...");
  device["status"] = new_status;
  str_device = "";
  device.printTo(str_device);
}

void send_heartbeat() {
  Serial.println("Enviando heartbeat...");
  Serial.println(str_device.c_str());
  client.publish("/DEVICE/HEARTBEAT", str_device.c_str());
}

void send_status() {
  Serial.println("Enviando atualização de status...");
  Serial.println(str_device.c_str());
  client.publish("/DEVICE/STATUS", str_device.c_str());
}

void send_new_device() {
  Serial.println("Enviando novo device...");
  Serial.println(str_device.c_str());
  client.publish("/DEVICE/NEW", str_device.c_str());
}

void callback(char* topic, byte* message, unsigned int length) {
  Serial.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n RECEBENDO MSG... \n TOPICO:");
  int msg_sinal = (int) message[0] - 48;
  if (strcmp(topic, switch_topic) == 0 ) {
    Serial.println(topic);
    if (msg_sinal == 1 ) {
      Serial.println("ligando...");
      digitalWrite(atoi(output), HIGH);
    } else if (msg_sinal == 0 ) {
      Serial.println("apagando...");
      digitalWrite(atoi(output), LOW);
    }
    update_status(msg_sinal);
    send_status();
  }
}

void reconnect() {
  while (!client.connected()) {
    Serial.println("Tentando conectar no MQTT Broker...");
    if (client.connect(device_mac.c_str())) {
      Serial.println("conectou...");
      client.subscribe(switch_topic);
      send_new_device();
    } else {
      Serial.println("falha ao conectar no MQTT Broker");
      Serial.println(client.state());
      Serial.println(" proxima tentativa em 5 segundos...");
      delay(5000);
    }
  }
}


void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis();
  if (now - last_heartbeat > 60000) { // 1 minuto
    last_heartbeat = now;
    send_heartbeat();
  }
}
